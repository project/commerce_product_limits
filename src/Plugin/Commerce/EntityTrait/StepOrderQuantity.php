<?php

namespace Drupal\commerce_product_limits\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the "maximum_order_quantity" trait.
 *
 * @CommerceEntityTrait(
 *   id = "step_order_quantity",
 *   label = @Translation("Step order quantity"),
 *   entity_types = {"commerce_product_variation"}
 * )
 */
class StepOrderQuantity extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['step_order_quantity'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Step quantity per order'))
      ->setSettings([
        'size' => 'normal',
        'unsigned' => TRUE,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 1,
      ]);

    return $fields;
  }

}
